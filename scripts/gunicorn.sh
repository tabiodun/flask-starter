#!/bin/bash

dockerize --timeout 240s -wait tcp://${MYSQL_DB_HOST:-db}:${MYSQL_DB_PORT:-3306}/ 2>&1
flask db upgrade
gunicorn -c gunicorn.conf.py wsgi:app --log-level INFO --reload