#!/bin/bash

dockerize --timeout 240s -wait tcp://${MQ_HOST:-rabbitmq}:${MQ_PORT:-5672}/ 2>&1
flask listen_for_messages