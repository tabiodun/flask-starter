import os
import pytest
from app import create_app, db
from app.models.user import User, UserRole

@pytest.fixture(scope="function")
def create_admin():
    user = User.create_user('admin@example.com', 'admin123', 'Admin', 'User', UserRole.ADMIN)
    db.session.add(user)
    db.session.commit()
    yield user


@pytest.fixture(scope="function")
def create_user():
    user = User.create_user('user_role@example.com', 'user123', 'Regular', 'User', UserRole.USER)
    db.session.add(user)
    db.session.commit()
    yield user

@pytest.fixture
def app():
    app = create_app('test.py')
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()