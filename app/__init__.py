import os
import pika
import logging

from flask.cli import with_appcontext

logging.basicConfig()

from flask import Flask, request, render_template, Response, jsonify
from flask_migrate import Migrate
from flask_restful import Api, Resource
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from flask_cors import CORS

from app.database import db
from app.parser import JsonApiParser
from app.resources.auth.resource import TokenResource, RegisterResource, AuthUserResource, GoogleAuthResource, \
    RevokedTokenResource
from app.resources.auth.schema import RegisterRequestSchema, TokenRequestSchema
from app.resources.base import json_api_error
from app.resources.users.resource import UserResource, UsersResource
from app.models.user import User, UserRole

class StripContentTypeMiddleware:
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if environ['REQUEST_METHOD'] == 'GET':
            try:
                del environ['CONTENT_TYPE']
            except KeyError:
                pass
        return self.app(environ, start_response)

def create_app(config='config.py'):
    app = Flask(__name__)
    app.wsgi_app = StripContentTypeMiddleware(app.wsgi_app)
    api = Api(app)
    app.config.from_pyfile(config)
    if os.environ.get('APPLICATION_SETTINGS', default=None):
        app.config.from_envvar('APPLICATION_SETTINGS')
    app.url_map.strict_slashes = False

    parser = JsonApiParser()

    app.config.update({
        'APISPEC_SPEC': APISpec(
            title='app',
            version='1',
            plugins=[MarshmallowPlugin()],
        ),
        'APISPEC_SWAGGER_URL': '/api/swagger.json',
        'APISPEC_WEBARGS_PARSER': parser
    })

    db.init_app(app)
    migrate = Migrate(app, db)
    CORS(app)

    api.add_resource(UserResource, '/api/users/<int:user_id>')
    api.add_resource(UsersResource, '/api/users')
    api.add_resource(TokenResource, '/auth/token')
    api.add_resource(RegisterResource, '/auth/register')
    api.add_resource(AuthUserResource, '/auth/user')
    api.add_resource(GoogleAuthResource, '/auth/google')
    api.add_resource(RevokedTokenResource, '/auth/token/revoke')

    docs = FlaskApiSpec(app)
    docs.register(UsersResource)
    docs.register(UserResource)
    docs.register(UsersResource)
    docs.register(TokenResource)
    docs.register(RegisterResource)
    docs.register(AuthUserResource)

    class HealthResource(Resource):
        def get(self):
            try:
                db.session.query("1").from_statement('SELECT 1').all()
                return {'status': 'up'}, 200
            except Exception as e:
                return {'status': 'down'}, 500

    api.add_resource(HealthResource, '/')

    class DocsResource(Resource):
        def get(self):
            return output_html(render_template('docs.html'), 200)

    api.add_resource(DocsResource, '/api/docs')

    def output_html(data, code, headers=None):
        resp = Response(data, mimetype='text/html', headers=headers)
        resp.status_code = code
        return resp

    @app.before_request
    def log_request():  # pragma: nocover
        if app.debug:
            import json
            request_data = request.json or request.form
            app.logger.info(json.dumps(request_data))

    @app.errorhandler(404)
    def page_not_found(e):
        return json_api_error(404, 'not_found', e.description), 404

    @app.errorhandler(422)
    def validation_error(e):
        return jsonify(e.data['messages']), 422

    @app.cli.command()
    def seed_db():
        user = User.create_user(
            'administrator@example.com',
            'admin123',
            'John',
            'Doe',
            UserRole.ADMIN)
        db.session.add(user)
        db.session.commit()

    def callback(ch, method, properties, body):
        app.logger.info(" [x] Received %r" % body)

    @app.cli.command()
    @with_appcontext
    def listen_for_messages():
        credentials = pika.PlainCredentials(app.config.get("MQ_USER"), app.config.get("MQ_PASS"))
        parameters = pika.ConnectionParameters(app.config.get("MQ_HOST"), 5672, '/', credentials)

        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue='app')

        channel.basic_consume(queue='app',
                      auto_ack=True,
                      on_message_callback=callback)

        app.logger.info('[*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()

    app.logger.setLevel(logging.INFO)

    return app
