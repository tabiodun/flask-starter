import pika
import json
from flask import current_app

def publish_queue_message(message):
    credentials = pika.PlainCredentials(current_app.config.get("MQ_USER"), current_app.config.get("MQ_PASS"))
    parameters = pika.ConnectionParameters(current_app.config.get("MQ_HOST"),
                                        5672,
                                        '/',
                                        credentials)

    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.queue_declare(queue='app')

    channel.basic_publish(exchange='',
                routing_key='app',
                body=json.dumps(message))
    current_app.logger.info("[x] Sent Message: {}".format(message))

    connection.close()
