from functools import wraps

import flask
import jwt
from flask import request, jsonify, make_response
from flask_apispec import MethodResource

from app.exceptions import TokenRevokedError
from app.models.user import User


def json_api_error(status_code, code, message):
    return jsonify({
        'errors': [
            {
                'status': status_code,
                'code': code,
                'detail': message
            }
        ]
    })


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_header = request.headers.get('Authorization')
        auth_token = None
        if auth_header:
            try:
                auth_token = auth_header.split(' ')[1]
            except IndexError:
                return make_response(json_api_error(
                    401, 'invalid_token', 'Bearer token malformed'), 401)

        try:
            user_id = User.decode_auth_token(auth_token)
        except jwt.ExpiredSignatureError:
            return make_response(json_api_error(
                401, 'token_expired', 'Bearer token expired. Log in again'), 401)
        except jwt.InvalidTokenError:
            return make_response(json_api_error(
                401, 'invalid_token', 'Bearer token is invalid'), 401)
        except TokenRevokedError:
            return make_response(json_api_error(
                401, 'token_revoked', 'Bearer token was revoked'), 401)

        user = User.get_user_by_id(user_id=user_id)
        flask.g.user_id = user.id
        flask.g.user_role = user.role
        flask.g.jwt_token = auth_token

        return func(*args, **kwargs)
    return wrapper


def authorize(allowed_roles):
    def actual_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if flask.g.user_role not in allowed_roles:
                return make_response(json_api_error(
                    403, 'unauthorized', 'You are not authorized to access this resource'), 403)
            return func(*args, **kwargs)
        return wrapper
    return actual_decorator


class BaseResource(MethodResource):
    pass


class AuthenticatedResource(BaseResource):
    decorators = [authenticate]


class UnauthenticatedResource(BaseResource):
    pass