from marshmallow import Schema, fields


class ErrorsSchema(Schema):
    status = fields.Integer()
    code = fields.String()
    detail = fields.String()


class ErrorSchema(Schema):
    errors = fields.Nested(ErrorsSchema)


class AuthErrorSchema(Schema):
    error = fields.String()
    type = fields.String()
