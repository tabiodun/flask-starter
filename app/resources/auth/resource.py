from uuid import uuid4

import requests
from urllib.parse import urlencode

import flask
from flask import make_response, jsonify, current_app
from flask_apispec import use_kwargs, marshal_with, doc
from requests import HTTPError
from webargs import fields

from app.constants import JWT_EXPIRY
from app.events import publish_queue_message
from app.resources.users.schema import UserSchema
from app.resources.auth.schema import TokenRequestSchema, RegisterRequestSchema, AccessTokenSchema
from app.resources.base import BaseResource, AuthenticatedResource

from app.models.user import User
from app.models.revoked_tokens import RevokedToken
from app.resources.errors.schema import AuthErrorSchema


class TokenResource(BaseResource):
    @use_kwargs(TokenRequestSchema(strict=True))
    @marshal_with(AccessTokenSchema, code=200)
    @marshal_with(AuthErrorSchema, code=404)
    @doc(description='Token Request')
    def post(self, **kwargs):
        user = User.get_user_by_email(kwargs['username'])
        if user and user.verify_password(password=kwargs['password']):
            return {
                'access_token': user.encode_auth_token(),
                'token_type': 'bearer',
                'expires_in': JWT_EXPIRY,
                'user_id': user.id,
                'user_role': user.role.value
            }

        return make_response(jsonify({
            'error': 'Email or password was incorrect.',
            'type': 'invalid_request',
        }), 404)


class RegisterResource(BaseResource):
    @use_kwargs(RegisterRequestSchema(strict=True))
    @marshal_with(AccessTokenSchema)
    @doc(description='Register User')
    def post(self, **kwargs):
        user = User.create_user(**kwargs)

        publish_queue_message({
            'type': 'USER REGISTERED',
            'user_id': user.id,
            'user_role': user.role.value
        })

        return {
            'access_token': user.encode_auth_token(),
            'token_type': 'bearer',
            "expires_in": JWT_EXPIRY,
            'user_id': user.id,
            'user_role': user.role.value
        }

class RevokedTokenResource(AuthenticatedResource):
    @doc(description='Revoke Token')
    def post(self):
        RevokedToken.revoke_token(flask.g.jwt_token)


class AuthUserResource(AuthenticatedResource):
    @marshal_with(UserSchema)
    @doc(description='Get Authenticated User')
    def get(self):
        user = User.get_user_by_id(flask.g.user_id)
        return user


class GoogleAuthResource(BaseResource):
    @use_kwargs({'access_token': fields.Str(required=True)})
    def post(self, **kwargs):
        params = {
            'access_token': kwargs.get('access_token'),
        }
        payload = urlencode(params)
        url = f'{current_app.config.get("GOOGLE_OAUTH_URL")}userinfo?{payload}'
        try:
            response = requests.get(url)
            response.raise_for_status()
        except HTTPError:
            return make_response(jsonify({
                'error': 'Error connecting to Google.',
                'type': 'internal_error',
            }), 500)

        email = response.json()['email']
        user = User.get_user_by_email(email)
        if not user:
            user = User.create_user(email=email, password=uuid4().hex)
        return {
            'access_token': user.encode_auth_token(),
            'token_type': 'bearer',
            'expires_in': JWT_EXPIRY,
            'user_id': user.id,
            'user_role': user.role.value,
            'provider': 'google-oauth2-bearer'
        }
