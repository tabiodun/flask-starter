from marshmallow import Schema, fields, validates, ValidationError, validate

from app.models.user import User


class TokenRequestSchema(Schema):
    username = fields.Email(load_only=True, required=True)
    password = fields.Str(
        load_only=True,
        required=True,
        validate=validate.Length(
            min=8))


class RegisterRequestSchema(Schema):
    email = fields.Email(load_only=True, required=True)
    password = fields.Str(
        load_only=True,
        required=True,
        validate=validate.Length(
            min=8))
    first_name = fields.Str(load_only=True, required=True)
    last_name = fields.Str(load_only=True, required=True)

    @validates('email')
    def validate_email(self, value):
        if User.email_exists(value):
            raise ValidationError('This email has already been registered')


class AccessTokenSchema(Schema):
    access_token = fields.Str()
    token_type = fields.Str()
    expires_in = fields.Integer()
    user_id = fields.Integer()
    user_role = fields.Str()
