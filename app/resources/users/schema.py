import flask
from marshmallow import validates, ValidationError, validate, pre_load
from marshmallow_enum import EnumField
from marshmallow_jsonapi import Schema, fields

from app.models.user import UserRole, User


class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    email = fields.Email(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    password = fields.Str(
        load_only=True,
        required=True,
        validate=validate.Length(
            min=8))
    role = EnumField(UserRole, by_value=True)
    created_at = fields.DateTime(dump_only=True)
    deleted = fields.Bool(dump_only=True)

    @pre_load
    def validate_email(self, data):
        if User.email_exists(data['email'], data.get('id')):
            raise ValidationError(
                'This email has already been registered', 'email')

    @validates('role')
    def validate_role(self, value):
        if flask.g.user_role not in (UserRole.USER_MANAGER, UserRole.ADMIN):
            raise ValidationError(
                'You do not have permission to update this attribute')

    class Meta:
        type_ = 'users'
        strict = True
