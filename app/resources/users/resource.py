import flask
from flask import make_response
from flask_apispec import marshal_with, use_kwargs, doc

from app.resources.base import AuthenticatedResource, authorize, json_api_error
from app.resources.errors.schema import ErrorSchema
from app.resources.users.schema import UserSchema

from app.models.user import User, UserRole


class UsersResource(AuthenticatedResource):
    validator = UserSchema

    @authorize((UserRole.USER_MANAGER, UserRole.ADMIN))
    @marshal_with(UserSchema(many=True))
    @doc(description='Get Users')
    def get(self):
        return User.query.all()

    @authorize((UserRole.USER_MANAGER, UserRole.ADMIN))
    @use_kwargs(UserSchema)
    @marshal_with(UserSchema)
    @doc(description='Create User')
    def post(self, **kwargs):
        user = User.create_user(**kwargs)
        return user


class UserResource(AuthenticatedResource):
    validator = UserSchema

    @authorize((UserRole.USER_MANAGER, UserRole.ADMIN, UserRole.USER))
    @marshal_with(UserSchema, code=200)
    @marshal_with(ErrorSchema, code=404)
    @marshal_with(ErrorSchema, code=403)
    @doc(description='Get User')
    def get(self, user_id):
        user = User.get_user_by_id(user_id)
        if not user:
            return make_response(json_api_error(
                404, 'not_found', 'User was not found'), 404)
        if flask.g.user_id != user_id and flask.g.user_role not in (
                UserRole.USER_MANAGER, UserRole.ADMIN):
            return make_response(json_api_error(
                403, 'forbidden', 'forbidden'), 403)
        return user

    @authorize((UserRole.USER_MANAGER, UserRole.ADMIN, UserRole.USER))
    @use_kwargs(UserSchema(partial=True))
    @marshal_with(UserSchema)
    @doc(description='Update User')
    def patch(self, user_id, **kwargs):
        user = User.get_user_by_id(user_id)
        if not user:
            return make_response(json_api_error(
                404, 'not_found', 'User was not found'), 404)
        if flask.g.user_id != user_id and flask.g.user_role not in (
                UserRole.USER_MANAGER, UserRole.ADMIN):
            return make_response(json_api_error(
                403, 'forbidden', 'forbidden'), 403)
        user.update(**kwargs)
        return user

    @authorize((UserRole.USER_MANAGER, UserRole.ADMIN))
    @marshal_with(None, code=204)
    @doc(description='Delete User')
    def delete(self, user_id):
        user = User.get_user_by_id(user_id)
        if not user:
            return make_response(json_api_error(
                404, 'not_found', 'User was not found'), 404)
        user.delete()
        return make_response('', 204)
