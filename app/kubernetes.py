import os

JWT_SECRET = os.environ.get("JWT_SECRET", default=None)
MYSQL_ROOT_USER = os.environ.get("MYSQL_ROOT_USER", default=None)
MYSQL_ROOT_PASSWORD = os.environ.get("MYSQL_ROOT_PASSWORD", default=None)
MYSQL_DB_PORT = os.environ.get("MYSQL_DB_PORT", default=None)
MYSQL_DB_HOST = os.environ.get("MYSQL_DB_HOST", default=None)
MQ_PORT = os.environ.get("MQ_PORT", default=None)
MQ_HOST = os.environ.get("MQ_HOST", default=None)
MQ_USER = os.environ.get("MQ_USER", default=None)
MQ_PASS = os.environ.get("MQ_PASS", default=None)

SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{MYSQL_ROOT_USER}:{MYSQL_ROOT_PASSWORD}@{MYSQL_DB_HOST}:{MYSQL_DB_PORT}/app'
SQLALCHEMY_TRACK_MODIFICATIONS = False
GOOGLE_OAUTH_URL = 'https://www.googleapis.com/oauth2/v1/'
