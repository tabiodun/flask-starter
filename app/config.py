import os

JWT_SECRET = os.environ.get("JWT_SECRET", default='super_secret')
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI",
    default='sqlite:////tmp/test.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False
GOOGLE_OAUTH_URL = 'https://www.googleapis.com/oauth2/v1/'
MQ_HOST = os.environ.get("MQ_HOST", default='rabbitmq')
MQ_PORT = os.environ.get("MQ_PORT", default='5672')
MQ_USER = os.environ.get("MQ_USER", default='rabbitmq')
MQ_PASS = os.environ.get("MQ_PASS", default='rabbitmq')
