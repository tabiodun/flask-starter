import datetime
import enum

import jwt
from flask import current_app
from flask_bcrypt import generate_password_hash, check_password_hash
from sqlalchemy.orm import relationship

from app.database import db
from app.exceptions import TokenRevokedError
from app.models.revoked_tokens import RevokedToken


class UserRole(enum.Enum):
    ADMIN = 'admin'
    USER_MANAGER = 'user_manager'
    USER = 'user'

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(120), nullable=False)
    last_name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(255), nullable=False)
    fb_id = db.Column(db.Integer)
    google_id = db.Column(db.Integer)
    msa_id = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)
    role = db.Column(db.Enum(UserRole), nullable=False, default=UserRole.USER)

    @property
    def deleted(self):
        return self.deleted_at is not None

    @classmethod
    def create_user(cls, email, password, first_name, last_name, role=UserRole.USER):
        password_hash = generate_password_hash(password=password)

        user = cls(
            email=email,
            password_hash=password_hash,
            first_name=first_name,
            last_name=last_name,
            role=role
        )

        db.session.add(user)
        db.session.commit()

        return user

    @classmethod
    def get_user_by_id(cls, user_id):
        return db.session.query(cls).filter(
            cls.id == user_id,
            cls.deleted_at.is_(None)
        ).one_or_none()

    @classmethod
    def get_user_by_email(cls, email):
        return db.session.query(cls).filter(
            cls.email == email,
            cls.deleted_at.is_(None)
        ).one_or_none()

    @classmethod
    def email_exists(cls, email, user_id=None):
        user = db.session.query(cls).filter(
            cls.email == email, cls.deleted_at.is_(None)).one_or_none()
        if user and user_id and user.id == int(user_id):
            return False
        return bool(user)

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if key == 'password':
                setattr(
                    self,
                    'password_hash',
                    generate_password_hash(
                        password=value))
            else:
                setattr(self, key, value)

        db.session.commit()

    def delete(self):
        self.deleted_at = datetime.datetime.utcnow()
        db.session.commit()

    def encode_auth_token(self):
        """
        Generates the Auth Token
        :return: string
        """
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'sub': self.id,
            'email': self.email,
            'role': self.role.value
        }
        return jwt.encode(
            payload,
            current_app.config['JWT_SECRET'],
            algorithm='HS256'
        ).decode('utf-8')

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        if RevokedToken.check_revoked(auth_token):
            raise TokenRevokedError

        payload = jwt.decode(
            auth_token,
            current_app.config['JWT_SECRET'],
            algorithms=['HS256'])

        return payload['sub']

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User %r>' % self.email
