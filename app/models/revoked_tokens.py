import datetime

from app import db
from app.exceptions import TokenRevokedError


class RevokedToken(db.Model):
    """
    Token Model for storing JWT tokens
    """
    __tablename__ = 'revoked_tokens'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(255), unique=True, nullable=False)
    revoked_on = db.Column(db.DateTime, nullable=False)

    @classmethod
    def revoke_token(cls, token):
        revoked_token = cls(
            token=token,
            revoked_on=datetime.datetime.now()
        )
        db.session.add(revoked_token)
        db.session.commit()


    @classmethod
    def check_revoked(cls, auth_token):
        revoked = cls.query.filter_by(token=str(auth_token)).first()
        if revoked:
            raise TokenRevokedError

    def __repr__(self):
        return '<id: token: {}'.format(self.token)