from app.models.user import UserRole

ALL_USER_ROLES = (UserRole.USER_MANAGER, UserRole.ADMIN, UserRole.USER)
JWT_EXPIRY = 28800