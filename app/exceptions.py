from jwt import PyJWTError


class TokenRevokedError(PyJWTError):
    pass