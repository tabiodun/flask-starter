import os

JWT_SECRET = os.environ.get("JWT_SECRET", default=None)
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI", default=None)
SQLALCHEMY_TRACK_MODIFICATIONS = False
GOOGLE_OAUTH_URL = 'https://www.googleapis.com/oauth2/v1/'
MQ_HOST = os.environ.get("MQ_HOST", default=None)
MQ_PORT = os.environ.get("MQ_PORT", default=None)
MQ_USER = os.environ.get("MQ_USER", default=None)
MQ_PASS = os.environ.get("MQ_PASS", default=None)
