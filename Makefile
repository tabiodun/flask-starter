.PHONY: test upload clean bootstrap

HOST=0.0.0.0
TEST_PATH=./

test: clean-pyc bootstrap
	sh -c '. env/bin/activate; py.test --verbose --color=yes $(TEST_PATH)'

test-docker: install
	find . -name "*.pyc" -delete
	py.test --verbose --color=yes $(TEST_PATH)

clean:
	rm -f MANIFEST
	rm -rf build dist

bootstrap: env
	env/bin/pip install -r requirements.txt
ifneq ($(wildcard test-requirements.txt),)
	env/bin/pip install -r test-requirements.txt
endif
	make clean

env:
	virtualenv env
	env/bin/pip install --upgrade pip
	env/bin/pip install --upgrade setuptools

clean-env:
	rm -rf env

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +

clean-build:
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info

wheel:
	python setup.py bdist_wheel

install:
	pip install -r requirements.txt

lint:
	flake8 --exclude=.tox

migrate:
	flask db migrate

db-upgrade:
	flask db upgrade

server: bootstrap
	env/bin/flask db upgrade
	env/bin/python run.py

server-docker: clean install db-upgrade
	python run.py

server-production: db-upgrade
	gunicorn -c gunicorn.conf.py wsgi:app

docker-run:
	docker run -it -w /usr/src/app -v "$(shell pwd)":/usr/src/app -p 5000:5000 python make server-docker

docker-test:
	docker run -it -w /usr/src/app -v "$(shell pwd)":/usr/src/app python make test-docker

create-tiller:
	kubectl --namespace kube-system create serviceaccount tiller
	kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
	kubectl --namespace kube-system patch deploy tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
	helm init --service-account tiller --history-max 200
	helm install --name jenkins-release stable/jenkins

gcloud:
	gcloud container clusters create flask-starter
	gcloud container clusters get-credentials flask-starter
	kubectl cluster-info
	kubectl delete daemonsets,replicasets,services,deployments,pods,rc,configmaps,secrets,pv,pvc,ingress,serviceaccount,clusterrolebinding --all
	kubectl apply -f .

tag:
	docker build -t gcr.io/flaskstarter-238119/flask-starter .
	docker push gcr.io/flaskstarter-238119/flask-starter

helm:
	helm install -f kubernetes/helm/values.yml --name jenkins-release stable/jenkins
	helm install --name rabbitmq stable/rabbitmq