import pytest
from marshmallow import ValidationError
from mock import patch

from app.models.user import UserRole
from app.resources.auth.schema import TokenRequestSchema, RegisterRequestSchema
from app.resources.users.schema import UserSchema

class TestSchema:
    def test_validate_token_request(self):
        """
        GIVEN a TokenRequestSchema
        WHEN a new Token is validated
        THEN check the email and password fields are defined correctly
        """
        user = TokenRequestSchema().load({"username": "john.doe@example.com", "password": "admin123"})
        assert user.data['username'] == 'john.doe@example.com'
        assert user.data['password'] == 'admin123'

    def test_validate_token_request_missing_field(self):
        """
        GIVEN a TokenRequestSchema
        WHEN a new Token is validated
        THEN check errors are correct when a field is missing
        """
        user = TokenRequestSchema().load({"password": "admin123"})
        assert user.errors['username'] == ['Missing data for required field.']
        assert user.data['password'] == 'admin123'

    def test_validate_token_request_validation_error(self):
        """
        GIVEN a RegisterRequestSchema
        WHEN a new Token is validated
        THEN check the password length is validated correctly
        """
        user = TokenRequestSchema().load({"username": "john.doe@example.com", "password": "admin12"})
        assert user.data['username'] == 'john.doe@example.com'
        assert user.errors['password'] == ['Shorter than minimum length 8.']


    def test_validate_register_request(self, app):
        """
        GIVEN a RegisterRequestSchema
        WHEN a new Token is validated
        THEN check the email and password fields are defined correctly
        """
        new_user = RegisterRequestSchema().load({"email": "john.doe@example.com", "password": "admin123"})
        assert new_user.data['email'] == 'john.doe@example.com'
        assert new_user.data['password'] == 'admin123'

    def test_validate_register_request_missing_field(self, app):
        """
        GIVEN a RegisterRequestSchema
        WHEN a new Token is validated
        THEN check errors are correct when a field is missing
        """
        new_user = RegisterRequestSchema().load({"password": "admin123"})
        assert new_user.errors['email'] == ['Missing data for required field.']
        assert new_user.data['password'] == 'admin123'

    def test_validate_register_request_validation_error(self, app):
        """
        GIVEN a RegisterRequestSchema
        WHEN a new Token is validated
        THEN check the password length is validated correctly
        """
        new_user = RegisterRequestSchema().load({"email": "john.doe@example.com", "first_name": "John", "last_name": "Doe", "password": "admin12"})
        assert new_user.data['email'] == 'john.doe@example.com'
        assert new_user.errors['password'] == ['Shorter than minimum length 8.']

    @patch('flask.g')
    def test_validate_user(self, g, app):
        """
        GIVEN a UserSchema
        WHEN a new User is validated
        THEN check the fields are defined correctly
        """
        g.user_role = UserRole.USER_MANAGER
        user = UserSchema().load({"data": {"attributes": {"email": "john.doe@example.com", "first_name": "John", "last_name": "Doe", "password": "admin123"}, "type": "users"}})
        assert user.data['email'] == 'john.doe@example.com'
        assert user.data['password'] == 'admin123'


    @patch('flask.g')
    def test_validate_user_update_role(self, g, app):
        """
        GIVEN a UserSchema
        WHEN a new User is validated with a privileged role
        THEN check the fields are defined correctly
        """
        g.user_role = UserRole.USER_MANAGER
        user = UserSchema(partial=True).load({"data": {"attributes": {"email": "john.doe@example.com", "password": "admin123", "role": UserRole.USER.value}, "type": "users"}})
        assert user.data['email'] == 'john.doe@example.com'
        assert user.data['password'] == 'admin123'
        assert user.data['role'] == UserRole.USER

    def test_validate_user_missing_field(self, app):
        """
        GIVEN a UserSchema
        WHEN a new User is validated
        THEN check errors are correct when a field is missing
        """
        with pytest.raises(ValidationError) as e:
            user = UserSchema().load(
                {"data": {"attributes": {"email": "john.doe@example.com", "first_name": "John", "last_name": "Doe"}, "type": "users"}})

        assert str(e.value) == "{'password': ['Missing data for required field.']}"

    @patch('flask.g')
    def test_validate_user_validation_error(self, g, app):
        """
        GIVEN a UserSchema
        WHEN a new User is validated
        THEN check that setting roles are validated properly
        """
        with pytest.raises(ValidationError) as e:
            g.user_role = UserRole.USER
            user = UserSchema().load({"data": {
                "attributes": {"email": "john.doe@example.com", "password": "admin123", "first_name": "John", "last_name": "Doe", "role": UserRole.USER.value},
                "type": "users"}})

        assert str(e.value) == "{'role': ['You do not have permission to update this attribute']}"