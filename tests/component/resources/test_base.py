import datetime

import jwt
from flask import current_app

from app.models.revoked_tokens import RevokedToken

class TestBaseResource:
    def test_get_auth_token_expired(self, app, create_admin):
        payload = {
            'exp': datetime.datetime.utcnow() - datetime.timedelta(days=0, seconds=5),
            'iat': datetime.datetime.utcnow(),
            'sub': create_admin.id,
            'email': create_admin.email,
            'role': create_admin.role.value
        }
        jwt_token = jwt.encode(
            payload,
            current_app.config['JWT_SECRET'],
            algorithm='HS256'
        ).decode('utf-8')

        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 401
        assert response.json == {
            'errors': [{
                'status': 401,
                'code': 'token_expired',
                'detail': 'Bearer token expired. Log in again'
            }]
        }

    def test_get_auth_token_invalid(self, app, create_admin):
        payload = {
            'exp': datetime.datetime.utcnow() - datetime.timedelta(days=0, seconds=5),
            'iat': datetime.datetime.utcnow(),
            'sub': create_admin.id,
            'email': create_admin.email,
            'role': create_admin.role.value
        }
        jwt_token = jwt.encode(
            payload,
            'JWT_SECRET',
            algorithm='HS256'
        ).decode('utf-8')

        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 401
        assert response.json == {
            'errors': [{
                'code': 'invalid_token',
                'detail': 'Bearer token is invalid',
                'status': 401
            }]
        }

    def test_get_auth_token_malformed(self, app, create_admin):
        jwt_token = '1'

        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'{jwt_token}'
            }
        )
        assert response.status_code == 401
        assert response.json == {
            'errors': [{
                'code': 'invalid_token',
                'detail': 'Bearer token malformed',
                'status': 401
            }]
        }

    def test_get_auth_token_revoked(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        RevokedToken.revoke_token(jwt_token)

        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 401
        assert response.json == {
            'errors': [{
                'status': 401,
                'code': 'token_revoked',
                'detail': 'Bearer token was revoked'
            }]
        }
