import datetime

import jwt
from flask import current_app
from mock import patch
from requests import HTTPError

from app import db
from app.models.user import UserRole, User


class TestApp:
    def test_page_not_found(self, app, create_admin):
        response = app.test_client().get(
            '/api/cheese'
        )
        assert response.status_code == 404
        assert response.json == {
            'errors': [{
                'code': 'not_found',
                'detail': 'The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.',
                'status': 404
            }]
        }