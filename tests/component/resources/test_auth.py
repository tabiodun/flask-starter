from marshmallow import fields

from app.models.revoked_tokens import RevokedToken
from app.constants import JWT_EXPIRY
from app.models.user import User


class TestAuthResource:
    def test_auth_token(self, app, create_admin):
        response = app.test_client().post(
            '/auth/token',
            data={
                'username': 'admin@example.com',
                'password': 'admin123'
            },
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        )
        assert response.status_code == 200
        assert response.json == {
            'access_token': create_admin.encode_auth_token(),
            'token_type': 'bearer',
            'expires_in': JWT_EXPIRY,
            'user_id': create_admin.id,
            'user_role': create_admin.role.value
        }

    def test_auth_token_fails(self, app, create_admin):
        response = app.test_client().post(
            '/auth/token',
            data={
                'username': 'admin@example.com',
                'password': 'admin1234'
            },
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        )
        assert response.status_code == 404
        assert response.json == {
            'error': 'Email or password was incorrect.',
            'type': 'invalid_request',
        }

    def test_auth_register(self, app, create_admin):
        response = app.test_client().post(
            '/auth/register',
            data={
                'email': 'admin2@example.com',
                'password': 'admin123',
                'first_name': 'John',
                'last_name': 'Doe'
            }
        )
        assert response.status_code == 200
        user = User.get_user_by_id(2)

        assert response.json == {
            'access_token': user.encode_auth_token(),
            'token_type': 'bearer',
            'expires_in': JWT_EXPIRY,
            'user_id': user.id,
            'user_role': user.role.value
        }

    def test_auth_register_email_exists(self, app, create_admin):
        response = app.test_client().post(
            '/auth/register',
            data={
                'email': 'admin@example.com',
                'password': 'admin123'
            }
        )
        assert response.status_code == 422


    def test_auth_user(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().get(
            '/auth/user',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 200
        assert response.json == {
            'data': {
                'type': 'users',
                'attributes': {
                    'role': 'admin',
                    'deleted': False,
                    'email': 'admin@example.com',
                    'first_name': 'Admin',
                    'last_name': 'User',
                    'created_at': fields.DateTime().serialize('created_at', create_admin)
                },
                'id': 1
            }
        }

    def test_auth_user_unauthenticated(self, app, create_admin):
        response = app.test_client().get('/auth/user',)
        assert response.status_code == 401

    def test_auth_revoke_token(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().post(
            '/auth/token/revoke',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 200
        assert RevokedToken.query.filter_by(token=str(jwt_token)).one_or_none()