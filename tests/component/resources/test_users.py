import json

from marshmallow import fields

from app import db
from app.models.user import User, UserRole


class TestUserResource:
    def test_get_users(self, app, create_admin):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 200
        assert response.json == {
            'data': [{
                'type': 'users',
                'attributes': {
                    'role': 'admin',
                    'deleted': False,
                    'email': 'admin@example.com',
                    'first_name': 'Admin',
                    'last_name': 'User',
                    'created_at': fields.DateTime().serialize('created_at', create_admin)
                },
                'id': 1
            },
                {
                    'type': 'users',
                    'attributes': {
                        'role': 'user',
                        'deleted': False,
                        'email': 'user@example.com',
                        'first_name': 'Regular',
                        'last_name': 'User',
                        'created_at': fields.DateTime().serialize('created_at', user)
                    },
                    'id': 2
                }]
        }

    def test_get_users_not_authorized(self, app, create_user):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        jwt_token = create_user.encode_auth_token()
        response = app.test_client().get(
            '/api/users',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 403

    def test_get_user(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().get(
            '/api/users/1',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 200
        assert response.json == {
            'data': {
                'type': 'users',
                'attributes': {
                    'role': 'admin',
                    'deleted': False,
                    'email': 'admin@example.com',
                    'first_name': 'Admin',
                    'last_name': 'User',
                    'created_at': fields.DateTime().serialize('created_at', create_admin)
                },
                'id': 1
            }
        }

    def test_get_user_not_authorized(self, app, create_user):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)

        user2 = User.create_user('user2@example.com', 'user123',  'Regular', 'User2', UserRole.USER)
        db.session.add(user2)
        db.session.commit()

        db.session.add(user)
        db.session.commit()
        jwt_token = create_user.encode_auth_token()
        response = app.test_client().get(
            '/api/users/2',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 403

    def test_get_user_not_found(self, app, create_admin):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().get(
            '/api/users/11',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 404

    def test_delete_user(self, app, create_admin):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        assert User.get_user_by_id(2) is not None

        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().delete(
            '/api/users/2',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 204
        assert User.get_user_by_id(2) is None

    def test_delete_user_not_authorized(self, app, create_user):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        jwt_token = create_user.encode_auth_token()
        response = app.test_client().delete(
            '/api/users/1',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 403

    def test_delete_user_not_found(self, app, create_admin):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().delete(
            '/api/users/11',
            headers={
                'Authorization': f'Bearer {jwt_token}'
            }
        )
        assert response.status_code == 404

    def test_create_user(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().post(
            '/api/users',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'user@example.com',
                        'password': 'user1234',
                        'first_name': 'John',
                        'last_name': 'Doe'
                    }

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 200
        user = User.get_user_by_id(2)
        assert response.json == {
            'data': {
                'type': 'users',
                'attributes': {
                    'role': 'user',
                    'deleted': False,
                    'email': 'user@example.com',
                    'first_name': 'John',
                    'last_name': 'Doe',
                    'created_at': fields.DateTime().serialize('created_at', user)
                },
                'id': 2
            }
        }

    def test_create_user_email_exists(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().post(
            '/api/users',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'admin@example.com',
                        'password': 'user1234',
                        'first_name': 'John',
                        'last_name': 'Doe',
                    }

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 422

    def test_create_user_unauthorized(self, app, create_user):
        jwt_token = create_user.encode_auth_token()
        response = app.test_client().post(
            '/api/users',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'user@example.com',
                        'password': 'user1234',
                        'first_name': 'John',
                        'last_name': 'Doe',
                    }

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 403

    def test_update_user(self, app, create_admin):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        assert user.email == "user@example.com"
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().patch(
            '/api/users/2',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'user+edited@example.com',
                        'password': 'user1234'
                    },
                    'id': 2

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 200
        assert user.email == "user+edited@example.com"

    def test_update_user_not_found(self, app, create_admin):
        jwt_token = create_admin.encode_auth_token()
        response = app.test_client().patch(
            '/api/users/2',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'user+edited@example.com',
                        'password': 'user1234'
                    },
                    'id': 2

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 404

    def test_update_user_unauthorized(self, app, create_user):
        user = User.create_user('user@example.com', 'user123', 'Regular', 'User', UserRole.USER)
        db.session.add(user)
        db.session.commit()
        assert user.email == "user@example.com"
        jwt_token = create_user.encode_auth_token()
        response = app.test_client().patch(
            '/api/users/2',
            data=json.dumps({
                'data': {
                    'type': 'users',
                    'attributes': {
                        'email': 'user+edited@example.com',
                        'password': 'user1234'
                    },
                    'id': 2

                }
            }),
            headers={
                'Authorization': f'Bearer {jwt_token}',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 403
